# CentOS + httpd + wsgi(py3) + flask + sqlite

### Aplicacion

Se desarrollo una aplicacion simple donde un usuario puede registrarse, loguearse y desloguearse.

Dicha imagen se encuentra en el repositorio de Docker Hub.

Para correr la aplicacion SIN CLONAR el proyecto, es decir, 'pulleando' la imagen del repositorio publico:
```
docker run -dit --name flaskLogin -p 8084:80 diegobernalistea/tpfinal
```
<br />

### Base de Datos

El archivo database.db ya essta "inicializado" con la siguiente estructura:

```
sqlite> .tables
user
sqlite> .schema user
CREATE TABLE user (
        id INTEGER NOT NULL,
        username VARCHAR(20) NOT NULL,
        password VARCHAR(80) NOT NULL,
        PRIMARY KEY (id),
        UNIQUE (username)
);
```

Parado en el mismo directorio donde se encuentra 'inicializadorDB.py' y teniendo las librerias necesarias instaladas:

```
python3
from inicializadorDB import db
db.create_all()
```

Se ve de esta forma:
```
[root@lexx SQLiteFlask]# python3
Python 3.6.8 (default, Nov 16 2020, 16:55:22)
[GCC 4.8.5 20150623 (Red Hat 4.8.5-44)] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> from inicializadorDB import db
/usr/local/lib/python4.6/site-packages/flask_sqlalchemy/__init__.py:852: UserWarning: Neither SQLALCHEMY_DATABASE_URI nor SQLALCHEMY_BINDS is set. Defaulting SQLALCHEMY_DATABASE_URI to "sqlite:///:memory:".
  'Neither SQLALCHEMY_DATABASE_URI nor SQLALCHEMY_BINDS is set. '
/usr/local/lib/python3.6/site-packages/flask_sqlalchemy/__init__.py:873: FSADeprecationWarning: SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and will be disabled by default in the future.  Set it to True or False to suppress this warning.
  'SQLALCHEMY_TRACK_MODIFICATIONS adds significant overhead and '
>>> db.create_all()
>>> #listo
...
[root@lexx SQLiteFlask]#
```
<br/>

Parea inicializar entonces se esta utilizando inicializadorDB.py llamandolo sin la extension desde la consola de python3

---

### Comandos utiles

Ingresar a la base de datos
```
docker exec -it flaskLogin sqlite3 database.db
```

sqlite CLI:
```
.help
.databases
.tables
.schema TABLE
.fullschema
.indexes TABLE
```

Ver los errores de apache:
```
docker exec -it flaskLogin cat /var/log/httpd/error.log
```

<br/>
